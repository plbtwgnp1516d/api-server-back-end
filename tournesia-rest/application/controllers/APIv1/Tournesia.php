<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Tournesia extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

		$this->load->model('User_model');
		$this->load->model('Votes_model');
		$this->load->model('Konten_model');
    }
    
	public function signUp_post(){
		$message = [
			'status'=>1,
			'message'=>'Added User',
		];
		$email=$this->post('email');
		$password=$this->post('password');
		
		$this->User_model->SignUp($email,$password);
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function login_post(){
		$email=$this->post('email');
		$password=$this->post('password');
		
		$userData = $this->User_model->Login($email,$password);
		
		if($userData != NULL){
			$message = [
				'userData' => $userData,
				'status' => 1,
				'message' => 'Login Succeed',
			];
		}else{
			$message = [
				'userData' => 'User not Found',
				'status' => 0,
				'message' => 'Login Not Succeed',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function userInfo_get(){
		$email=$this->get('email');
		
		$userData = $this->User_model->GetUserInfo($email);
		
		if($userData != NULL){
			$message = [
				'userData' => $userData,
				'status' => 1,
				'message' => 'All User Data Received',
			];
		}else{
			$message = [
				'userData' => 'User not Found',
				'status' => 0,
				'message' => 'User Data Retrieve Failed',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function totalVotes_get()
    {
        $email = $this->get('email');
		
		if($email != NULL){
			$totalVotes = $this->User_model->GetTotalVotes($email);
			
			if($totalVotes == NULL){
				$message = [
					'totalVotes' => 0,
					'status' => 0,
					'message' => 'Get Total Votes Succeed',
				];
			}else{
				$message = [
					'totalVotes' => $totalVotes,
					'status' => 1,
					'message' => 'Get Total Votes Succeed',
				];
			}
			$this->set_response($message, REST_Controller::HTTP_CREATED);
		}
    }
	
	public function redeemChallenge_post(){
		$email = $this->post('email');
		$challengeno = $this->post('challengeno');
		$addmedal = $this->post('addmedal');
		
		$userData = $this->User_model->RedeemChallenge($email,$challengeno,$addmedal);
		
		if($userData != NULL){
			$message =[
				'userData' => $userData,
				'status' => 1,
				'message' => 'Point(s) Added',
			];
		}else{
			$message =[
				'userData' => 'Point not Added',
				'status' => 1,
				'message' => 'User Not Found',
			];
		}
		
		$this->set_response($message,REST_Controller::HTTP_CREATED);
	}
	
	public function redeemRewards_post(){
		$email = $this->post('email');
		$minusmedal = $this->post('minusmedal');
		$namahadiah = $this->post('namahadiah');
		
		$userData = $this->User_model->RedeemRewards($email,$minusmedal,$namahadiah);
		
		if($userData != NULL && $minusmedal>0){
			$message =[
				'userData' => $userData,
				'status' => 1,
				'message' => 'Succesfully Redeem Reward',
			];
		}else if($userData == NULL || $minusmedal < 0){
			$message =[
				'userData' => 'failed to redeem',
				'status' => 0,
				'message' => 'Redeem Reward not Success',
			];
		}
		
		$this->set_response($message,REST_Controller::HTTP_CREATED);
	}
	
	public function voteUp_post(){
		$email = $this->post('email');
		$id_wisata = $this->post('id_wisata');
		
		$message = [
			'status'=>1,
			'message'=>'Vote Up Success',
		];
		
		$this->Votes_model->VoteUp($email,$id_wisata);
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function voteDown_post(){
		$email = $this->post('email');
		$id_wisata = $this->post('id_wisata');
		
		$message = [
			'status'=>1,
			'message'=>'Vote Down Success',
		];
		
		$this->Votes_model->VoteDown($email,$id_wisata);
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function voteNormal_post(){
		$email = $this->post('email');
		$id_wisata = $this->post('id_wisata');
		
		$message = [
			'status'=>1,
			'message'=>'Vote to Normal Success',
		];
		
		$this->Votes_model->VoteNormal($email,$id_wisata);
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function addKonten_post(){
		$message = [
			'status'=>1,
			'message'=>'Added Konten',
		];
		$email=$this->post('email');
		$nama=$this->post('nama');
		$detail=$this->post('detail');
		$judul=$this->post('judul');
		$gambar=$this->post('gambar');
		$tags=$this->post('tags');
		$lokasi=$this->post('lokasi');
		$namagambar=$this->post('namagambar');
		
		$this->Konten_model->AddKonten($email,$nama,$detail,$judul,$gambar,$tags,$lokasi,$namagambar);
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function kontenAll_get(){
		$email=$this->get('email');
		
		$kontenData = $this->Konten_model->GetKonten($email);
		
		if($kontenData != NULL){
			$message = [
				'kontenData' => $kontenData,
				'status' => 1,
				'message' => 'All Konten Data Received',
			];
		}else{
			$message = [
				'kontenData' => 'Konten not Found',
				'status' => 0,
				'message' => 'Konten Data Retrieve Failed',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function kontenHot_get(){
		$email=$this->get('email');
		
		$kontenData = $this->Konten_model->GetKontenHot($email);
		
		if($kontenData != NULL){
			$message = [
				'kontenData' => $kontenData,
				'status' => 1,
				'message' => 'All Konten Data Received',
			];
		}else{
			$message = [
				'kontenData' => 'Konten not Found',
				'status' => 0,
				'message' => 'Konten Data Retrieve Failed',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function getHistory_get(){
		$email=$this->get('email');
		
		$historyData = $this->User_model->GetUserHistory($email);
		
		if($historyData != NULL){
			$message = [
				'historyData' => $historyData,
				'status' => 1,
				'message' => 'All History Data Received',
			];
		}else{
			$message = [
				'kontenData' => 'History not Found',
				'status' => 0,
				'message' => 'History Data Retrieve Failed',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Wisata extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

		$this->load->model('User_model');
		$this->load->model('Votes_model');
		$this->load->model('Konten_model');
    }
    
	
	public function konten_get(){
		$apikey=$this->get('api_key');
		
		if($this->User_model->Authenticate($apikey)){
			$kontenData = $this->Konten_model->GetKontenPublic();
			$message = [
				
				'kontenData' => $kontenData,
				'status' => 200,
				'message' => 'Berhasil',
			];
		}else{
			$message = [
				'kontenData' => 'Akses tidak valid',
				'status' => 101,
				'message' => 'Gagal',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function kontenbyTags_get(){
		$tags=$this->get('tags');
		$apikey=$this->get('api_key');
		
		if($this->User_model->Authenticate($apikey)){
			$kontenData = $this->Konten_model->GetKontenPublicTags($tags);
			$message = [
				
				'kontenData' => $kontenData,
				'status' => 200,
				'message' => 'Berhasil',
			];
		}else{
			$message = [
				'kontenData' => 'Akses tidak valid',
				'status' => 101,
				'message' => 'Gagal',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
	public function kontenbyLokasi_get(){
		$lokasi=$this->get('lokasi');
		$apikey=$this->get('api_key');
		
		if($this->User_model->Authenticate($apikey)){
			$kontenData = $this->Konten_model->GetKontenPublicLokasi($lokasi);
			$message = [
				
				'kontenData' => $kontenData,
				'status' => 200,
				'message' => 'Berhasil',
			];
		}else{
			$message = [
				'kontenData' => 'Akses tidak valid',
				'status' => 101,
				'message' => 'Gagal',
			];
		}
		
		$this->set_response($message, REST_Controller::HTTP_CREATED);
	}
	
}
<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Konten_model extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	
	function AddKonten($Email, $Nama_Wisata, $Detail_Wisata, $Judul_Konten, $Gambar_Wisata, $Tags, $Lokasi, $Nama_Gambar)
	{
		$konten = array(
			'Email' => $Email,
			'Nama_Wisata' => $Nama_Wisata,
			'Detail_Wisata' => $Detail_Wisata,
			'Judul_Konten' => $Judul_Konten,
			'Gambar_Wisata' => 'http://hnwtvc.com/tournesiapic/'.$Nama_Gambar,
			'Tags' => $Tags,
			'Lokasi' => $Lokasi
		);
		
	        $imsrc = base64_decode($Gambar_Wisata);
	        $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/tournesiapic/'.$Nama_Gambar.'', 'w');
	        fwrite($fp, $imsrc);
	        
		$this->db->insert('konten_wisata',$konten);
	
	}

	function GetKonten($LoggedEmail)
	{
		$this->db->select();
		$this->db->from('konten_wisata')->order_by('Created_at', 'DESC');
		
		$query = $this->db->get();
		
		$result =array();
		
		foreach($query->result() as $row) {
			
			$this->db->select('flag')->where('Email', $LoggedEmail)->where('ID_Wisata', $row->ID_Wisata);
			$this->db->from('user_votes');
			$flagquery = $this->db->get();
			
			$result[] = array(
			'ID_Wisata' => $row->ID_Wisata,
			'Email' => $row->Email,
			'Nama_Wisata' => $row->Nama_Wisata,
			'Detail_Wisata' => $row->Detail_Wisata,
			'Judul_Konten' => $row->Judul_Konten,
			'Tipe_Wisata' => $row->Tipe_Wisata,
			'Gambar_Wisata' => $row->Gambar_Wisata,
			'Jumlah_Votes' => $this->db->where('flag', "2")->where('ID_Wisata', $row->ID_Wisata)->from('user_votes')->count_all_results(),
			'Tags' => $row->Tags,
			'Lokasi' => $row->Lokasi, 
			'Created_at' => $row->Created_at,
			'Flag'=>$flagquery->result()
			);
		}
		
		return $result;

	}
	
	function GetKontenHot($LoggedEmail)
	{
		$this->db->select();
		$this->db->from('konten_wisata')->order_by('Jumlah_Votes', 'DESC');
		
		$query = $this->db->get();
		
		$result =array();
		
		foreach($query->result() as $row) {
			
			$this->db->select('flag')->where('Email', $LoggedEmail)->where('ID_Wisata', $row->ID_Wisata);
			$this->db->from('user_votes');
			$flagquery = $this->db->get();
			
			$result[] = array(
			'ID_Wisata' => $row->ID_Wisata,
			'Email' => $row->Email,
			'Nama_Wisata' => $row->Nama_Wisata,
			'Detail_Wisata' => $row->Detail_Wisata,
			'Judul_Konten' => $row->Judul_Konten,
			'Tipe_Wisata' => $row->Tipe_Wisata,
			'Gambar_Wisata' => $row->Gambar_Wisata,
			'Jumlah_Votes' => $this->db->where('flag', "2")->where('ID_Wisata', $row->ID_Wisata)->from('user_votes')->count_all_results(),
			'Tags' => $row->Tags,
			'Lokasi' => $row->Lokasi, 
			'Created_at' => $row->Created_at,
			'Flag'=>$flagquery->result()
			);
		}
		
		return $result;

	}
	
	function GetKontenPublic()
	{
		$this->db->select();
		$this->db->from('konten_wisata')->order_by('Jumlah_Votes', 'DESC');
		
		$query = $this->db->get();
		
		$result =array();
		
		foreach($query->result() as $row) {
			
			$result[] = array(
			'Nama_Wisata' => $row->Nama_Wisata,
			'Detail_Wisata' => $row->Detail_Wisata,
			'Judul_Konten' => $row->Judul_Konten,
			'Gambar_Wisata' => $row->Gambar_Wisata,
			'Jumlah_Votes' => $this->db->where('flag', "2")->where('ID_Wisata', $row->ID_Wisata)->from('user_votes')->count_all_results(),
			'Tags' => $row->Tags,
			'Lokasi' => $row->Lokasi, 
			);
		}
		
		return $result;
	}
	
	function GetKontenPublicTags($tags)
	{
		$this->db->select();
		$this->db->from('konten_wisata')->like('Tags',$tags);
		
		$query = $this->db->get();
		
		$result =array();
		
		foreach($query->result() as $row) {
			
			$result[] = array(
			'Nama_Wisata' => $row->Nama_Wisata,
			'Detail_Wisata' => $row->Detail_Wisata,
			'Judul_Konten' => $row->Judul_Konten,
			'Gambar_Wisata' => $row->Gambar_Wisata,
			'Jumlah_Votes' => $this->db->where('flag', "2")->where('ID_Wisata', $row->ID_Wisata)->from('user_votes')->count_all_results(),
			'Tags' => $row->Tags,
			'Lokasi' => $row->Lokasi, 
			);
		}
		
		return $result;
	}
	
	
	function GetKontenPublicLokasi($lokasi)
	{
		$this->db->select();
		$this->db->from('konten_wisata')->like('Lokasi',$lokasi);
		
		$query = $this->db->get();
		
		$result =array();
		
		foreach($query->result() as $row) {
			
			$result[] = array(
			'Nama_Wisata' => $row->Nama_Wisata,
			'Detail_Wisata' => $row->Detail_Wisata,
			'Judul_Konten' => $row->Judul_Konten,
			'Gambar_Wisata' => $row->Gambar_Wisata,
			'Jumlah_Votes' => $this->db->where('flag', "2")->where('ID_Wisata', $row->ID_Wisata)->from('user_votes')->count_all_results(),
			'Tags' => $row->Tags,
			'Lokasi' => $row->Lokasi, 
			);
		}
		
		return $result;
	}
	
	
}
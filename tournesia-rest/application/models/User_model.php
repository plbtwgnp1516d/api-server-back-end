<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}

	function GetUserInfo($LoggedEmail)
	{
		$this->db->select()->where('Email', $LoggedEmail);
		$this->db->from('user_app');
		
		$query = $this->db->get();
		return $query->result();
	}
	
	function SignUp($Email,$Password){
		$data = array(
				'Email' => $Email,
				'Password' => $Password,
			);
			
		$this->db->insert('user_app',$data);
	}
	
	function Login($Email,$Password){
		$this->db->select('*');
		$this->db->from('user_app');
		$this->db->where('Email',$Email);
		$this->db->where('Password',$Password);
		
		$query = $this->db->get();
		
		if($query ->num_rows()==1)
		{	
			return $query->result();	
		}
		else
		{
			return false;
		}
	}
	
	function Authenticate($apikey){
		$this->db->select('*');
		$this->db->from('user_api');
		$this->db->where('api_key',$apikey);
		
		$query = $this->db->get();
		
		if($query ->num_rows()==1)
		{	
			$this->db->set('total_request', 'total_request+1', FALSE);
			$this->db->where('api_key', $apikey);
			$this->db->update('user_api');
			return true;	
		}
		else
		{
			return false;
		}
	}
	
	function GetTotalVotes($LoggedEmail){
		$this->db->select_sum('Jumlah_Votes');
		$this->db->from('konten_wisata');
		$this->db->where('Email',$LoggedEmail);
		
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			$totalrate = $query->row("Jumlah_Votes");
			return $totalrate;
		}
		else{
			return 0;
		}
		
	}
	
	function RedeemChallenge($LoggedEmail,$challengeno,$addpoint){
		
		$this->db->select('*');
		$this->db->from('user_app');
		$this->db->where('Email',$LoggedEmail);
		
		$querypoints = $this->db->get();
		
		if($querypoints->num_rows()>0){
			$points_total = $querypoints->row("Jumlah_Medal");
		
			if($challengeno == 1){
				
				$data=array(
					'Challenge1' => true,
					'Jumlah_Medal' => $points_total + $addpoint,
				);
			}else if($challengeno == 2){
				$data=array(
					'Challenge2' => true,
					'Jumlah_Medal' => $points_total + $addpoint,
				);
			}else if($challengeno == 3){
				$data=array(
					'Challenge3' => true,
					'Jumlah_Medal' => $points_total + $addpoint,
				);
			}else if($challengeno == 4){
				$data=array(
					'Challenge4' => true,
					'Jumlah_Medal' => $points_total + $addpoint,
				);
			}else if($challengeno ==5){
				$data=array(
					'Challenge5' => true,
					'Jumlah_Medal' => $points_total + $addpoint,
				);
			}
			
			$this->db->update('user_app',$data,array('Email'	=> $LoggedEmail));
			
			$this->db->select('*');
			$this->db->from('user_app');
			$this->db->where('Email',$LoggedEmail);
			
			$query = $this->db->get();
			
			if($query ->num_rows()==1)
			{	
				return $query->result();	
			}
			else
			{
				return false;
			}
		}else{
			return false;
		}
		
	}
	
	function GetUserHistory($LoggedEmail)
	{
		$this->db->select()->where('Email', $LoggedEmail);
		$this->db->from('user_history');
		
		$query = $this->db->get();
		return $query->result();
	}
	
	
	function RedeemRewards($LoggedEmail,$minuspoint,$namahadiah){
		$this->db->select('*');
		$this->db->from('user_app');
		$this->db->where('Email',$LoggedEmail);
		
		$querypoints = $this->db->get();
		
		if($querypoints->num_rows() > 0){
		
					$hdata = array(
						'Email' => $LoggedEmail,
						'Hadiah' => $namahadiah,
					);
			
					$this->db->insert('user_history',$hdata);
					
			$pointstotal = $querypoints->row("Jumlah_Medal");
			
			$pointstotal = $pointstotal - $minuspoint;
			
			if($pointstotal<0){
				return NULL;
			}else{
				$data=array(
						'Jumlah_Medal' => $pointstotal,
					);
				
				$this->db->update('user_app',$data,array('Email'	=> $LoggedEmail));	
				
				$this->db->select('*');
				$this->db->from('user_app');
				$this->db->where('Email',$LoggedEmail);
				
				$query = $this->db->get();
				
				if($query ->num_rows()==1)
				{	
					return $query->result();	
				}
				else
				{
					return false;
				}
			}
		}
	}
}
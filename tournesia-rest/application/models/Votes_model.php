<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Votes_model extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	
	function VoteUp($Email,$ID_Wisata){
		
		$this->db->select('*');
		$this->db->from('user_votes');
		$this->db->where('Email',$Email);
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$query = $this->db->get();
		
		$this->db->select('Jumlah_Votes');
		$this->db->from('konten_wisata');
		$this->db->where('ID_Wisata',$ID_Wisata);
			
		if($query ->num_rows()==0)
		{	
			$data = array(
				'Email' => $Email,
				'ID_Wisata' => $ID_Wisata,
				'Flag' => 2,
			);
			
			$this->db->insert('user_votes',$data);	
		}
		else
		{
			$data=array(	
				'Flag' => 2,
			);
			
			$this->db->update('user_votes',$data,array('Email'	=> $Email,'ID_Wisata' => $ID_Wisata));
		}
		
		$datacount=array(	
				'Jumlah_Votes' => $this->db->where('flag', "2")->where('ID_Wisata', $ID_Wisata)->from('user_votes')->count_all_results(),
		);
		$this->db->update('konten_wisata',$datacount,array('ID_Wisata' => $ID_Wisata));
		
		$this->db->update('konten_wisata',$addTotalRate,array('ID_Wisata' => $ID_Wisata));	
	}
	
	function VoteDown($Email,$ID_Wisata){
		
		$this->db->select('*');
		$this->db->from('user_votes');
		$this->db->where('Email',$Email);
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$query = $this->db->get();
		
		$this->db->select('Jumlah_Votes');
		$this->db->from('konten_wisata');
		$this->db->where('ID_Wisata',$ID_Wisata);
			
		if($query ->num_rows()==0)
		{	
			$data = array(
				'Email' => $Email,
				'ID_Wisata' => $ID_Wisata,
				'Flag' => 1,
			);
			
			$this->db->insert('user_votes',$data);	
		}
		else
		{
			$data=array(	
				'Flag' => 1,
			);
			
			$this->db->update('user_votes',$data,array('Email'	=> $Email,'ID_Wisata' => $ID_Wisata));
		}
		
				$datacount=array(	
				'Jumlah_Votes' => $this->db->where('flag', "2")->where('ID_Wisata', $ID_Wisata)->from('user_votes')->count_all_results(),
		);
		$this->db->update('konten_wisata',$datacount,array('ID_Wisata' => $ID_Wisata));
		
		$this->db->update('konten_wisata',$addTotalRate,array('ID_Wisata' => $ID_Wisata));	
	}
	
	function VoteNormal($Email,$ID_Wisata){
		
		$this->db->select('*');
		$this->db->from('user_votes');
		$this->db->where('Email',$Email);
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$query = $this->db->get();
		
		$this->db->select('Flag');
		$this->db->from('user_votes');
		$this->db->where('Email',$Email);
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$flag_rate_res = $this->db->get();
		$flag_rate_result = $flag_rate_res->row("Flag");
		
		if($flag_rate_result == 1){
			$flagcondition = 1;
		}else if($flag_rate_result == 2){
			$flagcondition = -1;
		}
		
		$this->db->select('Jumlah_Votes');
		$this->db->from('konten_wisata');
		$this->db->where('ID_Wisata',$ID_Wisata);
		
			
		if($query ->num_rows()==0)
		{	
			$data = array(
				'Email' => $Email,
				'ID_Wisata' => $ID_Wisata,
				'Flag' => 0,
			);
			
			$this->db->insert('rate',$data);	
		}
		else
		{
			$data=array(	
				'Flag' => 0,
			);
			
			$this->db->update('user_votes',$data,array('Email'	=> $Email,'ID_Wisata' => $ID_Wisata));
		}
		
				$datacount=array(	
				'Jumlah_Votes' => $this->db->where('flag', "2")->where('ID_Wisata', $ID_Wisata)->from('user_votes')->count_all_results(),
		);
		$this->db->update('konten_wisata',$datacount,array('ID_Wisata' => $ID_Wisata));
		
		$this->db->update('konten_wisata',$addTotalRate,array('ID_Wisata' => $ID_Wisata));	
	}
}